type Input = {
  nameInput: string;
  setNameInput: React.Dispatch<React.SetStateAction<string>>;
  cancelEditing: () => void;
};

export function Input({ nameInput, setNameInput, cancelEditing }: Input) {
  return (
    <div>
      <input value={nameInput} onChange={(e) => setNameInput(e.target.value)} />
      <button onClick={cancelEditing}>x</button>
    </div>
  );
}
