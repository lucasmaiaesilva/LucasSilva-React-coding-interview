import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';
import { useState } from 'react';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { Input } from '@components/Input';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person: { name, email },
  sx,
}) => {
  const [isEditingName, setIsEditingName] = useState(true);
  const [nameInput, setNameInput] = useState(name);
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          {!isEditingName ? (
            <Typography variant="subtitle1" lineHeight="1rem">
              {nameInput}
            </Typography>
          ) : (
            <Input
              nameInput={nameInput}
              setNameInput={setNameInput}
              cancelEditing={() => setIsEditingName(false)}
            />
          )}
          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};
